Studentnummer:331139
Naam:Sam Bergman 

Hoe open je de Terminal in Visual Studio Code?
	# CTRL + Shift + '
	# Terminal -> New Terminal 

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# git status 

Wat is het commando van een multiline commit message?
	#git commit -m "Begin commit message" -m "rest van message 
	>>volgende regel van commit"

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	#9
>git status
>git add pad/naar/bestand
>gitstatus
>git commit

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	#git revert HEAD~3
	#git reset --soft/--mixed/--hard Head~2
	#git revert HEAD~3
